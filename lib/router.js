Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading'
});

/*
 * logged to console:
 *
 * 1. in waitOn
 * 2. in onBeforeAction
 * 3. in data: not ready
 * 4. in onBeforeAction
 * 5. in action
 * 6. in onAfterAction
 * 7. in data: ready
 */

Router.map(function() {
  return this.route('home', {
    path: '/',
    waitOn: function() {          // runs once
      console.log('in waitOn');
      return [Meteor.subscribe('people')];
    },
    onBeforeAction: function() {  // runs multiple times
      console.log('in onBeforeAction');
    },
    data: function() {            // runs multiple times
      if (this.ready()) {
        console.log('in data: ready');
      } else {
        console.log('in data: not ready');
      }
      return {
        'people': People.find()
      };
    },
    action: function() {          // runs once
      console.log('in action');
      this.render();
    },
    onAfterAction: function() {   // runs once
      console.log('in onAfterAction');
    }
  });
});

Router.onBeforeAction('loading');
